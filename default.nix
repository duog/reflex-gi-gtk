{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
, lib ? pkgs.lib
, haskell ? pkgs.haskell
, compiler ? "ghc902"
, haskellPackages ? if compiler == null then pkgs.haskellPackages else haskell.packages.${compiler}
, inNixShell ? false
, shellForPackages ? ps: [ps.reflex-gi-gtk]
}: with lib; with haskell.lib; let
  hp = let
    extension = self: super: {
      # patch = doJailbreak (dontCheck super.patch);
      reflex-gi-gtk = doJailbreak (self.callCabal2nix "reflex-gi-gtk" ./. {});
      this-shell = self.shellFor {
        packages = shellForPackages;
        buildInputs = [
          haskellPackages.haskell-language-server
          haskellPackages.cabal-install
          haskellPackages.cabal2nix
        ];
      };
    };
    in haskellPackages.extend extension;
in if inNixShell then hp.this-shell else hp
