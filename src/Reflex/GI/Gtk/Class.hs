-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE ConstraintKinds, FlexibleContexts #-}

{-|
Description : Interface class for reactive GTK
Copyright   : Sven Bartscher 2020
License     : MPL-2.0
Maintainer  : sven.bartscher@weltraumschlangen.de
Stability   : experimental
-}
module Reflex.GI.Gtk.Class (MonadReflexGtk) where

import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (MonadIO)
import Reflex ( Adjustable
              , MonadHold
              , NotReady
              , PerformEvent
              , Performable
              , PostBuild
              , TriggerEvent
              )
import Reflex.Host.Class (ReflexHost)
import Reflex.GI.Gtk.Input (MonadGtkSource)
import Reflex.GI.Gtk.Run.Class (MonadRunGtk)

-- | This class declares the interface provided for use in monadic
-- code for constructing reactive networks. The most notable
-- implementation is 'Reflex.GI.Gtk.Host.ReflexGtk'.
type MonadReflexGtk t m = ( MonadIO m
                          , MonadFix m
                          , MonadRunGtk m
                          , ReflexHost t
                          , MonadHold t m
                          , MonadGtkSource t m
                          , NotReady t m
                          , TriggerEvent t m
                          , PostBuild t m
                          , Adjustable t m
                          , PerformEvent t m
                          , MonadIO (Performable m)
                          , MonadRunGtk (Performable m)
                          , MonadFix (Performable m)
                          , MonadHold t (Performable m)
                          )
