-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

{-# LANGUAGE DataKinds, GADTs, AllowAmbiguousTypes, RankNTypes #-}
{-# LANGUAGE FlexibleContexts, FlexibleInstances, ConstraintKinds #-}

{-|
Description : Host and helpers for running reactive GTK interfaces with Reflex FRP
Copyright   : Sven Bartscher 2020
License     : MPL-2.0
Maintainer  : sven.bartscher@weltraumschlangen.de
Stability   : experimental

This module provides the most important functions to create reactive
interfaces using reflex and gi-gtk.

'runReflexGtk' provides a reflex host, i.e. the top level entry point
to start constructing your reactive network. It relies on the User
providing a 'GI.Gtk.Application' to run. Running a GTK application
using the older 'GI.Gtk.init' and 'GI.Gtk.main' is currently not
supported.

__Warning__: While 'runReflexGtk' provides access to
'Control.Monad.IO.Class.MonadIO' you should be careful not to execute
GTK-related functions using 'Control.Monad.IO.Class.liftIO'. GTK
functions expect to be called from the same OS thread that GTK was
initialized in. 'runReflexGtk' internally starts mutliple threads and
any code you write might not be run in the correct thread to execute
GTK functions. To make sure that your GTK function calls are performed
in the correct thread, use 'runGtk' which has the same type as
'Control.Monad.IO.Class.liftIO' and always makes sure that the lifted
IO action is run in the correct context to properly call GTK
functions.

Reactive inputs can be obtained with the help of 'eventOnSignal0' and
similar helpers defined in "Reflex.GI.Gtk.Input". Reactive values can
be rendered as properties of widgets using 'sink'. The submodules of
"Reflex.GI.Gtk.Widget" also provide specific helpers for specific
widgets, such as 'sinkBox' or 'sinkBin' to render dynamic widgets into
'GI.Gtk.Box'es or 'GI.Gtk.Bin's respectively.
-}
module Reflex.GI.Gtk
  ( -- * Running
    runReflexGtk
  , ReflexGtk
  , MonadReflexGtk
    -- * Executing IO in GTK context
  , runGtk
  , runGtk_
  , runGtkPromise
    -- * Obtaining input from GTK sources
  , module Reflex.GI.Gtk.Input
    -- * Outputting reactive values to GTK widgets
  , MonadGtkSink
  , sink
  , ReactiveAttrOp(..)
    -- * Helpers for specific widgets
  , module Reflex.GI.Gtk.Widget
  ) where

import Reflex.GI.Gtk.Class (MonadReflexGtk)
import Reflex.GI.Gtk.Host ( ReflexGtk
                          , runReflexGtk
                          )
import Reflex.GI.Gtk.Input
import Reflex.GI.Gtk.Output ( MonadGtkSink
                            , ReactiveAttrOp(..)
                            , sink
                            )
import Reflex.GI.Gtk.Run.Class ( runGtk
                               , runGtk_
                               , runGtkPromise
                               )
import Reflex.GI.Gtk.Widget
