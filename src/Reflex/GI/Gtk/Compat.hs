{-# language ScopedTypeVariables, ImplicitParams, RankNTypes #-}

module Reflex.GI.Gtk.Compat(on) where

import qualified Data.GI.Base.Signals (on)
import Data.GI.Base.Signals ( HaskellCallbackType
                            , SignalHandlerId
                            , SignalInfo
                            , SignalProxy
                            )
import Data.GI.Base (GObject)

on :: forall object info. (GObject object, SignalInfo info)
  => object
  -> SignalProxy object info
  -> HaskellCallbackType info
  -> IO SignalHandlerId
on ob sp hct = let
  hct' :: (?self::object) => HaskellCallbackType info
  hct' = hct
  in Data.GI.Base.Signals.on ob sp hct'
